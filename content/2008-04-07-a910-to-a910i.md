+++
title = "A910 to A910i"
aliases = ["2008/04/a910-to-a910i.html"]
date = "2008-04-07T13:03:00Z"
[taxonomies]
tags = ["A910", "A910i", "AP/BP-problem", "modding"]
categories = ["projects", "shortform",]
authors = ["peter"]
+++

Well, I after I had an idea last night, how creating a mixed firmware could work, I had to work on it&mdash;of course (though I don't have any time to do such stuff). I put some files from CG44 to CG34 and symlinked them&mdash;and I got my A910 flashed, it even starts up.
But apparently, AP is not able to get a connection to BP, as there is no signal strength and battery indicator loaded. Now I'll need to fix that issue... afterwards, it might be possible to get an A910__i__ by flashing an A910. If I'll be successful (if you have got any ideas, please contact me (by commenting here)), I'll of course post it here.
