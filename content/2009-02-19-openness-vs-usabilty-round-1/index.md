+++
title = "\"Openness\" vs. \"Usabilty\" - Round 1"
aliases = ["2009/02/19/openness-vs-usability-round-1.html"]
author = "peter"
date = "2009-02-19T14:01:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "free software", "gta02", "GTA03", "Neo Freerunner", "openmoko", "openness", "usability"]
categories = ["software", "commentary"]
authors = ["peter"]
+++

_The headline might be confusing, actually this is something like a &#8220;virtual&#8221; fight, which is ought to point out the differences between an open device like my recently aquired &#8220;Neo FreeRunner&#8221; and the T-Mobile G1 I have for a short time, too. In a way this is a kind of reply to&mdash;or at least inspired by <a href="http://www.datenfreihafen.org/%7Estefan/weblog/archives/2009/02/18/index.html#e2009-02-18T22_37_15.txt">this post</a> by FSO/OpenMoko/OpenEZX developer Stefan Schmidt._
<!-- more -->
<center><img src="openvsusable.jpg" style="margin:10px;" alt="Open vs. usable. Neo FreeRunner on the left, HTC Dream/T-Mobile G1 on the right." /></center>

Yesterday I sat in a lecture at University and wanted to view a PDF with some exercises for statistics on one of my mobile devices, instead of just printing it&mdash;as I hate to waste paper (actually I could write several pages about my relation to paper as a medium and the waste of it, but I won't do this to you right now ;) ).

The task was as simple as viewing a PDF. As my first soldering attempt on the FRs (and close to first soldering attempt at all, soldered the last time on a soldering workshop on a birthday party approx. 10 years ago)  AUX switch hadn't proved to be successful, I had to use the G1 whether I wanted or not.

I knew that there isn't a pre-installed PDF reader, so I went to the &#8220;Android Market&#8221;, searching for PDF: Attempt failed, no working PDF reader there, judging by comments. Starting the browser, searching Google for &#8220;android pdf reader&#8221;: Some results. So I chose &#8220;Multi Reader&#8221;. After installing the app, opening the PDF took some time. And guess what: The app started to read out the PDF featuring a text-like view which messed up some details. So I went on with searching. When the time I spent on that was already half an hour and I was close to printing the stuff, I had the idea I should have had before: Just using a web pdf viewer. Searched Google and <a href="http://view.samurajdata.se/">chose the first result</a>. Works. Great!

This was on the G1, which is supposed to be an easy to use platform. It indeed is, but currently it still lacks many applications I really would like to see (at best as free software)&mdash;I guess that there will be a bunch of commercial solutions as soon as the Android Market features paid apps, but anyway, guys: PDF is needed that much, OK, there is that preview function for attachments, but it doesn't help, as you are unable to send mails with documents attached (you're only able to attach photos in Google Mail App ATM; besides: While testing it with an invoice I got recently, the preview just crashed and nothing else)&#8230;.

If I'd had a working FreeRunner with a good distribution allowing me an easy internet connection (didn't set up anything like that on one of the distributions I tried yet (ANDROID (I will do a write up about ANDROID soon), SHR unstable, FSO MS5, Qtopia 4.3.x)), it would habe been a no brainer, a simple opkg / apt-get install epdfview at worst. So in this case, the FreeRunner with it's distributions is far ahead.<br />Considering other cases, it isn't as much as it could be. Looking at distributions like SHR (ok, I only tried the unstable, so can't really judge it) or FSO MS5 (I know that it's just a framework demo), there've been some points really annoying me. First of all the theme that the really great &#8220;Illume&#8221; uses&mdash;it is too fat (and power-hungry, as s3c2442 armv4t + xglamo isn't a powerhouse) if you ask me. I have read that there is a lighter (==better) theme used on OpenMokos 2008.12&mdash;but this distribution has other disadvantages. Then there is the lack of a finger (or even stylus friendly) browser which doesn't force you to use scrollbars (beside old and possible insecure Minimo), though there certainly is such software, e.g. gecko-based <a href="http://packages.ubuntu.com/de/intrepid/midbrowser">midbrowser</a> or coming up <a href="http://en.wikipedia.org/wiki/Fennec_%28browser%29">fennec</a> &mdash;but (even considering that it might run rather slow (midori e.g. isn't bad at speed)) there is no thing like that in the package repositorys. Well, I have high hopes for the future in an enlightenment/webkit based browser like ewww&mdash;but the situation right now is rather sad.

And besides that: All popular distributions (besides Debian which packages are meanwhile often rather fat or have dependencies one could argue on) the package reposoritys I've had a look at are rather poor of packages, didn't see vpnc (i'd need it for university) or even abiword anywhere (looking at GTA02 as pocket computer (absurd? think of USB host mode!) with a phone in it this is a huge lack)&mdash;of course this isn't surprising as SHR e.g. is still rather new and hasn't released a version stating to be stable yet&mdash;but I can just hope that maybe even more collaboration efforts between distributions make this situation better&mdash;I know how to cross compile things, but hey, more simplicity is always nice.

Anyway, comparing G1 and FreeRunner the latter is somehow ahead in my internal judgement, simply because it's so open and because i can imagine a good or even bright future due to platform efforts like FSO and Paroli&mdash;I guess that there will be good and stable distributions for the GTA02 without much drawbacks this year&mdash;probably just at the time when GTA03 will hit the distributors (though i am not sure that a GTA03 on sale this year is realistic, as there have been no shots of hardware prototypes yet (well, couldn't find any)).

Concluding all this weird bunch of words I wrote, openly admitting that this conclusion is maybe to general for a &#8220;first round of a fight&#8221;:

While you are always depend on some operators/manufacturers on a &#8220;normal platform&#8221; (besides the platforms security is cracked), you have close to endless chances on an open platform, limited only to your phantasy, creativity and ability and the hardware's performance.
