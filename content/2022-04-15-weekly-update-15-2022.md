+++
title = "Weekly Update (15/2022): Frameworks, Components and a place that tracks PinePhone Pro software status"
draft = false
date = "2022-04-15T21:50:00Z"
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Librem 5","PINE64 Community Update","Kupfer","Tow-Boot",]
categories = ["weekly update"]
authors = ["peter"]
+++

Another calm week, where improvement happens by many tiny steps, done one at a time.
<!-- more -->

_Commentary in italics._

### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#39: Sending Locations](https://thisweek.gnome.org/posts/2022/04/twig-39/). _Many nice improvements to existing and upcoming convergent apps plus Aarch64 flatpak runners, awesome!_
* James Westman: [Next Steps for Blueprint](https://www.jwestman.net/2022/04/12/next-steps-for-blueprint.html)

#### Plasma/Maui ecosystem
* KDE Community: [KDE Ships Frameworks 5.93.0](https://pointieststick.com/2022/04/08/this-week-in-kde-a-feast-for-the-eyes/).
* Nate Graham: [This week in KDE: a feast for the eyes](https://pointieststick.com/2022/04/08/this-week-in-kde-a-feast-for-the-eyes/).
* Nate Graham: [This week in KDE: Stable desktop icons and even better gestures](https://pointieststick.com/2022/04/14/this-week-in-kde-stable-desktop-icons-and-even-better-gestures/). _Great progress!_
* Volker Krause: [Requiring passing unit tests on KDE’s CI](https://www.volkerkrause.eu/2022/04/09/kde-ci-required-unit-tests.html).
* Qt Dev Blog: [Qt 6.3 released](https://www.qt.io/blog/qt-6.3-released).
  * Phoronix: [Qt 6.3 Released With Improved Wayland Support, Qt Language Server Module](https://www.phoronix.com/scan.php?page=news_item&px=Qt-6.3-Released).
* KDE Eco: [GCompris 2.4 Release: Improved Data Efficiency Conserves Network Bandwidth, Saves Energy](https://eco.kde.org/blog/2022-04-13-gcompris-data-efficiency/).
* Felipe Kinoshita: [LoadingMessage component for Kirigami](https://fhek.gitlab.io/en/loadingmessage-component-for-kirigami/).

#### Sailfish OS
* [Sailfish Community News, 7th April, Vanha Rauma](https://forum.sailfishos.org/t/sailfish-community-news-7th-april-vanha-rauma/11017).

#### Distro news
* Mobian blog: [Moving to Tow-Boot: Towards Device-Independant Images](https://blog.mobian.org/posts/2022/03/30/universal-images/)
* The founder of [Kupfer](https://gitlab.com/kupfer), an Arch Linux ARM based distribution for several (mainly Snapdragon 845 powered) Linux Phones, Kate, stepped down this week. Details can be read in [Kupfer's Matrix room](https://matrix.to/#/!nRCCSrbkLQIuPZwkKD:matrix.org/$ys6jbFlfe9LzyzoBIAvRnRcS13B9XasABYE1yzuamwQ?via=matrix.org&via=t2bot.io&via=postmarketos.org). _I hope that the project will be continued!_

### Worth noting
* Another [messaging product might come to Linux Phones](https://www.reddit.com/r/PINE64official/comments/u0dzs3/bringing_anonymous_decentralized_messenger_to/), soon. _Sadly, I did not manage to build the software on any of my ARM devices._
* [Healthy proof that the Librem 5 with postmarketOS can take pictures!](https://freeradical.zone/@craftyguy/108116610329681735)

### Worth reading

#### PINE64 Community Update
* PINE64: [April update: no more unicorns](https://www.pine64.org/2022/04/15/april-update-no-more-unicorns/). _Wireless earbuds, ElementaryOS mobile... interesting! Also, the PinePhone Pro Wiki Improvments are really great!_
  * Liliputing: [PineBuds are wireless earbuds with user-flashable firmware from open hardware maker Pine64 (coming… eventually?)](https://liliputing.com/2022/04/pinebuds-are-wireless-earbuds-with-user-flashable-firmware-from-open-hardware-maker-pine64-coming-eventually.html)

#### PinePhone Keyboard
* xnux.eu log (Megi): [PinePhone keyboard keymaps](https://xnux.eu/log/#066).

#### Upstream first
* Tobias Bernard for Purism: [How to be Upstream-First](https://puri.sm/posts/how-to-be-upstream-first/). _Great read!_

#### Fluff
* Purism: [Purism Cares about Environmental Impact](https://puri.sm/posts/purism-cares-about-environmental-impact/).

#### Chromium and Qt
* Phoronix: [Google Chrome/Chromium Experimenting With A Qt Back-End](https://www.phoronix.com/scan.php?page=news_item&px=Chromium-Qt-WIP). _Is this beneficial to Linux Phones? I don't know, but I don't think it can hurt._

### Worth watching
#### PINE64 Community Update
* PINE64: [April Update: No More Unicorns](https://www.youtube.com/watch?v=hl3lFWzFMco). _Another great synopsis by PizzaLovingNerd!_

#### PinePhone Pro In Depth
* nordkamp: [The PinePhone Pro is REALLY Exciting: Arch ARM, Plasma Mobile, Waydroid, SSH Overview](https://www.youtube.com/watch?v=IrP-X0ervDo). _Nice video!_

#### Droidian
* Scientific Perspective: [Droidian GNU/Linux | PHOSH | Multirom feat. AOSP | POCO F1](https://www.youtube.com/watch?v=D6H__yAqaqM).

#### Overviews
* Jeremy's Tech Channel: [Lets Talk about Open Source Phones, Mobile Open Source, Linux Phones, and What's Available!!!](https://www.youtube.com/watch?v=CupEgh8i91w). _What, Linux Phones are a thing?_

#### PineNote
* Danct12: [DOOM running on PineNote](https://www.youtube.com/watch?v=PQiHOjIUTiE).
* Danct12: [Windows XP on PineNote](https://www.youtube.com/watch?v=m_XRdHec154).

#### Accessory Privacy
* (RTP) Privacy Tech Tips: [Pinetime: Now Privacy Friendly (Bluetooth Can Be Disabled!)](https://www.youtube.com/watch?v=E5nRuCV1Yas).

#### Sxmo
* nephitejnf: [Giving SXMO a Real Go, One Day in and More to Come](https://www.youtube.com/watch?v=KIdaNqrdMS0). _Not new, but I somehow missed this earlier. Better late than never, am I right?_

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!
