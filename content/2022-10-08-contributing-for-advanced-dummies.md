+++
title = "Contributing for (advanced?) Dummies"
date = "2022-10-08T15:30:00Z"
draft = false 
[taxonomies]
tags = ["Tokodon", "Plasma Mobile", "git", "postmarketOS", "tinkering", "contributing",]
categories = ["software","howto",]
authors = ["peter"]
+++

*Like the [camera tinkering post](*[*https://linmob.net/playing-with-pinephone-video-recording/*](https://linmob.net/playing-with-pinephone-video-recording/)*) earlier, this is a post where I try to describe how I approach challenges when it comes to real world problems in the mobile Linux ecosystem. I hope it's somewhat helpful.*

Recently, after I noticing that Tootle had been archived, I re-evaluated Tokodon, the mobile friendly Mastodon client for smartphones.

<!-- more -->

## The problem

I vaguely recalled that Tokodon had had one bug that really annoyed me when I had last tried it a few weeks or months ago, and when using it I noticed that it was still there – unsurprisingly since I never had reported it: When composing a toot, the text would not wrap when reaching the end of the text field, but go on and on unless you would add a manual line break, which then would look bad in the resulting toot unless I would remove them.

Since I keep hitting any character limit a service has, I, unsurprisingly, was more likely to run into this than others. I was annoyed, but when I first encountered it while briefly trying out Plasma Mobile on the Pocophone F1 on a release that wasn't the latest, I just lazy-searched the project, did not find any issue or merge request on the topic and went "well, surely this is fixed in the latest release, I don't want to annoy people".

I then later discovered that this wasn't the case when re-evaluating Tokodon after Tootle's archival. So I figured: Ok, time to file an issue - but how do I describe this? I could not find the word, was stuck in my native German, thinking "Was heißt Zeilenumbruch auf Englisch?"

## Analysis

### This can't be to hard to fix, but which file(s) do I need to change

While wondering about this, I figured: Tokodon is build on Kirigami and QtQuick, these things both have been around for a while, there are apps that have text areas that don't show this behaviour, and generally any half-decent framework simply has this problem solved.

Knowing that Kirigami apps consist of some QML markup, which is not to far from my web programming knowledge and a C++ core, I assumed that this would likely be a thing that could be fixed in QML and thus something I might be able to do. 

I browsed the [repository](<https://invent.kde.org/network/tokodon>) and tried to find the file. I don't recall how I exactly went about this, I think I searched for "compose toot" and found translation files that led me to the file in question, [TootComposer.qml](<https://invent.kde.org/network/tokodon/-/blob/master/src/content/ui/TootComposer.qml>).

Reading it, I quickly knew that this was the [correct place](<https://invent.kde.org/network/tokodon/-/blob/master/src/content/ui/TootComposer.qml#L26>) (I recogniized the "What's new?" placeholder), and knowing next to nothing about QML, the kind of widget, QQC2.TextArea, seemed quite plausible.

### How to fix this?

I then wondered how to change this. Obviously, the correct approach would be to look for documentation, but I've often found that especially when you have no idea what you are doing, it would be easier to look actual code that did this correctly. I figured that what I had to look for where the terms `QQC2.TextArea` and `wrap`, and tried to search [invent.kde.org](http://invent.kde.org), only to quickly be frustrated by the imperfections of GitLab's search (mainly the non-existence of cross project code search). 

Knowing that multiple KDE projects are mirrored on GitHub, and having had quite good experiences with GitHub's search, I went to GitHub and found [these results](https://github.com/search?q=QQC2.TextArea+wrap&type=code) [^1], and picked [this one](<https://github.com/KDE/drkonqi-pk-debug-installer/blob/aa25b5e8b131207a34e9518ff111fdd94195c0bb/src/qml/ErrorPage.qml>). Could this be a one line fix? Time to try!

## Testing my fix (with postmarketOS)

Fortunately, I knew how I could try this. I had played with [aports/APKBUILD files before](<https://framagit.org/linmobapps/apkbuilds>), and had realized that including a patch was basically creating a patch file, putting it in the folder, and referencing the file name once, then run `pmbootstrap checksum tokodon` before building the package with that patch.

### Creating a patch

But how to generate a patchfile? There are multiple ways, and IMHO for simple stuff like this, the - as far as I know - simplest way is to

* `git clone` the original repository,
* edit the file you want to change, 
* `git add` that file,
* run `git commit -m "Descriptive commit message"` to commit your change,
* and then run `git format-patch -1` which will simply generate a patch file for your latest commit.

Boom, done!

So I did this, taking the then latest [Tokodon APKBUILD file](https://gitlab.alpinelinux.org/alpine/aports/-/commit/f8aaf9d6f1408660b3372138cfa403555f0588bb) (which I found via [repology.org](http://repology.org)), [modified it](<https://framagit.org/linmobapps/apkbuilds/-/commit/097fb3f511f59dda9d3b48904b1f60de8f570638>) to include my patch, and built and tried the resulting package as outlined [here](<https://framagit.org/linmobapps/apkbuilds#um-what-do-i-do-with-this>). And guess what? Problem solved!

## Creating a Merge Request

I then replicated the entire thing in this [merge request](<https://invent.kde.org/network/tokodon/-/merge_requests/39?commit_id=988d4200ca4bee19f6fbea8e968c94286ffbeea4>) - I was lazy and recreated the patch on [invent.kde.org](http://invent.kde.org) by forking the repo, editing the file and committing it to a new branch and then creating a merge request, where described the limitations of my testing and a little later it was merged. Yay!

### It's fixed now

And, because my timing was fortunate, my tiny little mini fix is now in the latest release of Tokodon, 22.09. So if you have a current release of Tokodon, what annoyed me, won't annoy you. 

## Conclusion 

Looking back, this was a really great experience. Obviously, not every issue can be fixed by pasting one random line from GitHub, but similar problems are more common than unicorns, especially in the nascent software ecosystem of #LinuxMobile, so maybe you'll come across one of these, too. I definitely recommend looking into finding a fix whenever you have the time to do so. In the worst case you're going to accomplish nothing, but you will always have learned something and will be able to come up with a better description of your issue.

_If you have questions or feedback, please [send me an email](mailto:contributing-dummy@linmob.net?subject=Feedback%20to%20%22Contributing%20for%20(advanced%3F)%20Dummies%22)!_

[^1]: Sadly, you need to be signed in to get results in cross-project code search on GitHub.
